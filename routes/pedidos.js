const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'Usando GET da rota pedidos'
    });
});

router.get('/:id', (req, res, next) => {
    const id = req.params.id;

    res.status(200).send({
        mensagem: 'Id Especial',
        id: id
    });

});

router.post('/', (req, res, next) => {

    const pedido = {
        id_produto: req.body.id_produto,
        quantidade: req.body.quantidade
    };

    res.status(201).send({
        mensagem: 'Usando POST da rota pedidos',
        pedidoCriado: pedido
    });
});

router.delete('/', (req, res, next) => {
    res.status(201).send({
        mensagem: 'Usando o DELETE da rota de pedidos',
    });
});

module.exports = router;