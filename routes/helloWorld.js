const express = require('express');
const router = express.Router();
const helloWorldController = require('../controllers/helloWorld-controller')

router.get('/', helloWorldController.helloWorld);

module.exports = router;