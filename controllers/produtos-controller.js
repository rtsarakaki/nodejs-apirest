const mysql = require('../mysql');

exports.getProdutos = async (req, res, next) => {
    try {
        const query = 'SELECT * FROM produtos';
        const result = await mysql.execute(query)
        const response = {
            quantidade: result.length,
            produtos: result.map(prod => {
                return {
                    id_produto: prod.id_produto,
                    nome: prod.nome,
                    preco: prod.preco,
                    imagem_produto: process.env.API_HOST + prod.imagem_produto,
                    request: {
                        tipo: 'GET',
                        descricao: 'Retorna um produto pela chave primária.',
                        url: process.env.API_HOST + 'produtos/' + prod.id_produto
                    }
                }
            })
        };

        return res.status(200).send({ response });
    }
    catch (error) {
        return res.status(500).send({ error: error });
    }
};

exports.getProdutosById = async (req, res, next) => {
    try {
        const query = 'SELECT * FROM produtos WHERE id_produto = ?';
        const result = await mysql.execute(query, [req.params.id])
        const response = {
            produtos: {
                id_produto: result[0].id_produto,
                nome: result[0].nome,
                preco: result[0].preco,
                imagem_produto: process.env.API_HOST + result[0].imagem_produto,
                usuario: req.usuario,
                request: {
                    tipo: 'GET',
                    descricao: 'Retorna todos os produtos.',
                    url: process.env.API_HOST + 'produtos'
                }
            }                    
        };

        return res.status(200).send({ response });
    }
    catch (error) {
        return res.status(500).send({ error: error });
    }
};

exports.insertProduto = async (req, res, next) => {
    try {
        const query = 'INSERT INTO produtos (nome, preco, imagem_produto) VALUES (?, ?, ?)';
        const result = await mysql.execute(query, [req.body.nome, req.body.preco, req.file.path]);
        const response = {
            mensagem: 'Produto inserido com sucesso!',
            produtoCriado: {
                id_produto: result.id_produto,
                nome: req.body.nome,
                preco: req.body.preco,
                request: {
                    tipo: 'GET',
                    descricao: 'Retorna todos os produtos.',
                    url: process.env.API_HOST + 'produtos'
                }
            }
        };
        return res.status(201).send({ response });
    }
    catch (error) {
        return res.status(500).send({ error: error });
    }
};

exports.updateProduto = async (req, res, next) => {
    try {
        const query = 'UPDATE produtos SET nome = ?, preco = ? WHERE id_produto = ?';
        await mysql.execute(query, [ req.body.nome, req.body.preco, req.body.id_produto ]);

        const response = {
            mensagem: 'Produto alterado com sucesso!',
            produtoAlterado: {
                id_produto: req.body.id_produto,
                nome: req.body.nome,
                preco: req.body.preco,
                request: {
                    tipo: 'GET',
                    descricao: 'Retorna todos os produtos.',
                    url: process.env.API_HOST + 'produtos'
                }
            }
        };
        return res.status(201).send({ response });
    }
    catch (error) {
        return res.status(500).send({ error: error });
    }
};

exports.deleteProduto = async (req, res, next) => {
    try {
        const query = 'DELETE FROM produtos WHERE id_produto = ?';
        const result = await mysql.execute(query, [req.body.id_produto]);

        if (result.affectedRows == 0) { return res.status(404).send({ mensagem: 'Nenhum produto foi excluído.', id_produto: req.body.id_produto })};

        const response = {
            mensagem: 'Produto excluído com sucesso!',
            produtoExcluido: {
                id_produto: req.body.id_produto,
                nome: req.body.nome,
                preco: req.body.preco,
                request: {
                    tipo: 'POST',
                    descricao: 'Insere um novo produto',
                    url: process.env.API_HOST + 'produtos',
                    body : {
                        nome: "String",
                        preco: "Number"
                    }
                }
            }
        };
        
        return res.status(201).send({ response });
    }
    catch (error) {
        return res.status(500).send({ error: error });
    }
};